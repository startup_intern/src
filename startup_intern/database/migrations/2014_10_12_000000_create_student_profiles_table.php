<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('first_name', 255);
            $table->string('last_name', 255);
            $table->integer('category_id');
            $table->integer('country_id');
            $table->text('university_name');
            $table->text('university_address');
            $table->text('photo');
            $table->text('resume');
            $table->text('video');
            $table->text('facebook');
            $table->text('linkedin');
            $table->text('github');
            $table->text('website');
            $table->text('skype_id');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_profiles');
    }
}
