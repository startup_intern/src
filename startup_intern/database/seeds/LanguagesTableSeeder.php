<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'name' => 'English'
        ]);
        DB::table('languages')->insert([
            'name' => 'Afar'
        ]);
        DB::table('languages')->insert([
            'name' => 'Abkhazian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Avestan'
        ]);
        DB::table('languages')->insert([
            'name' => 'Afrikaans'
        ]);
        DB::table('languages')->insert([
            'name' => 'Akan'
        ]);
        DB::table('languages')->insert([
            'name' => 'Amharic'
        ]);
        DB::table('languages')->insert([
            'name' => 'Aragonese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Arabic'
        ]);
        DB::table('languages')->insert([
            'name' => 'Assamese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Avaric'
        ]);
        DB::table('languages')->insert([
            'name' => 'Aymara'
        ]);
        DB::table('languages')->insert([
            'name' => 'Azerbaijani'
        ]);
        DB::table('languages')->insert([
            'name' => 'Bashkir'
        ]);
        DB::table('languages')->insert([
            'name' => 'Belarusian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Bulgarian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Bihari languages'
        ]);
        DB::table('languages')->insert([
            'name' => 'Bislama'
        ]);
        DB::table('languages')->insert([
            'name' => 'Bambara'
        ]);
        DB::table('languages')->insert([
            'name' => 'Bengali'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tibetan'
        ]);
        DB::table('languages')->insert([
            'name' => 'Breton'
        ]);
        DB::table('languages')->insert([
            'name' => 'Bosnian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Catalan; Valencian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Chechen'
        ]);
        DB::table('languages')->insert([
            'name' => 'Chamorro'
        ]);
        DB::table('languages')->insert([
            'name' => 'Corsican'
        ]);
        DB::table('languages')->insert([
            'name' => 'Cree'
        ]);
        DB::table('languages')->insert([
            'name' => 'Czech'
        ]);
        DB::table('languages')->insert([
            'name' => 'Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic'
        ]);
        DB::table('languages')->insert([
            'name' => 'Chuvash'
        ]);
        DB::table('languages')->insert([
            'name' => 'Welsh'
        ]);
        DB::table('languages')->insert([
            'name' => 'Danish'
        ]);
        DB::table('languages')->insert([
            'name' => 'German'
        ]);
        DB::table('languages')->insert([
            'name' => 'Divehi; Dhivehi; Maldivian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Dzongkha'
        ]);
        DB::table('languages')->insert([
            'name' => 'Ewe'
        ]);
        DB::table('languages')->insert([
            'name' => 'Greek, Modern (1453-)'
        ]);
        DB::table('languages')->insert([
            'name' => 'English'
        ]);
        DB::table('languages')->insert([
            'name' => 'Esperanto'
        ]);
        DB::table('languages')->insert([
            'name' => 'Spanish; Castilian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Estonian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Basque'
        ]);
        DB::table('languages')->insert([
            'name' => 'Persian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Fulah'
        ]);
        DB::table('languages')->insert([
            'name' => 'Finnish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Fijian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Faroese'
        ]);
        DB::table('languages')->insert([
            'name' => 'French'
        ]);
        DB::table('languages')->insert([
            'name' => 'Western Frisian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Irish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Gaelic; Scottish Gaelic'
        ]);
        DB::table('languages')->insert([
            'name' => 'Galician'
        ]);
        DB::table('languages')->insert([
            'name' => 'Guarani'
        ]);
        DB::table('languages')->insert([
            'name' => 'Gujarati'
        ]);
        DB::table('languages')->insert([
            'name' => 'Manx'
        ]);
        DB::table('languages')->insert([
            'name' => 'Hausa'
        ]);
        DB::table('languages')->insert([
            'name' => 'Hebrew'
        ]);
        DB::table('languages')->insert([
            'name' => 'Hindi'
        ]);
        DB::table('languages')->insert([
            'name' => 'Hiri Motu'
        ]);
        DB::table('languages')->insert([
            'name' => 'Croatian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Haitian; Haitian Creole'
        ]);
        DB::table('languages')->insert([
            'name' => 'Hungarian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Armenian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Herero'
        ]);
        DB::table('languages')->insert([
            'name' => 'Interlingua (International Auxiliary Language Association)'
        ]);
        DB::table('languages')->insert([
            'name' => 'Indonesian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Interlingue; Occidental'
        ]);
        DB::table('languages')->insert([
            'name' => 'Igbo'
        ]);
        DB::table('languages')->insert([
            'name' => 'Sichuan Yi; Nuosu'
        ]);
        DB::table('languages')->insert([
            'name' => 'Inupiaq'
        ]);
        DB::table('languages')->insert([
            'name' => 'Ido'
        ]);
        DB::table('languages')->insert([
            'name' => 'Icelandic'
        ]);
        DB::table('languages')->insert([
            'name' => 'Italian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Inuktitut'
        ]);
        DB::table('languages')->insert([
            'name' => 'Japanese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Javanese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Georgian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kongo'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kikuyu; Gikuyu'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kuanyama; Kwanyama'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kazakh'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kalaallisut; Greenlandic'
        ]);
        DB::table('languages')->insert([
            'name' => 'Central Khmer'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kannada'
        ]);
        DB::table('languages')->insert([
            'name' => 'Korean'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kanuri'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kashmiri'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kurdish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Komi'
        ]);
        DB::table('languages')->insert([
            'name' => 'Cornish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kirghiz; Kyrgyz'
        ]);
        DB::table('languages')->insert([
            'name' => 'Latin'
        ]);
        DB::table('languages')->insert([
            'name' => 'Luxembourgish; Letzeburgesch'
        ]);
        DB::table('languages')->insert([
            'name' => 'Ganda'
        ]);
        DB::table('languages')->insert([
            'name' => 'Limburgan; Limburger; Limburgish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Lingala'
        ]);
        DB::table('languages')->insert([
            'name' => 'Lao'
        ]);
        DB::table('languages')->insert([
            'name' => 'Lithuanian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Luba-Katanga'
        ]);
        DB::table('languages')->insert([
            'name' => 'Latvian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Malagasy'
        ]);
        DB::table('languages')->insert([
            'name' => 'Marshallese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Maori'
        ]);
        DB::table('languages')->insert([
            'name' => 'Macedonian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Malayalam'
        ]);
        DB::table('languages')->insert([
            'name' => 'Mongolian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Marathi'
        ]);
        DB::table('languages')->insert([
            'name' => 'Malay'
        ]);
        DB::table('languages')->insert([
            'name' => 'Maltese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Burmese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Nauru'
        ]);
        DB::table('languages')->insert([
            'name' => 'Bokmål, Norwegian; Norwegian Bokmål'
        ]);
        DB::table('languages')->insert([
            'name' => 'Ndebele, North; North Ndebele'
        ]);
        DB::table('languages')->insert([
            'name' => 'Nepali'
        ]);
        DB::table('languages')->insert([
            'name' => 'Ndonga'
        ]);
        DB::table('languages')->insert([
            'name' => 'Dutch; Flemish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Norwegian Nynorsk; Nynorsk, Norwegian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Norwegian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Ndebele, South; South Ndebele'
        ]);
        DB::table('languages')->insert([
            'name' => 'Navajo; Navaho'
        ]);
        DB::table('languages')->insert([
            'name' => 'Chichewa; Chewa; Nyanja'
        ]);
        DB::table('languages')->insert([
            'name' => 'Occitan (post 1500); Provençal'
        ]);
        DB::table('languages')->insert([
            'name' => 'Ojibwa'
        ]);
        DB::table('languages')->insert([
            'name' => 'Oromo'
        ]);
        DB::table('languages')->insert([
            'name' => 'Oriya'
        ]);
        DB::table('languages')->insert([
            'name' => 'Ossetian; Ossetic'
        ]);
        DB::table('languages')->insert([
            'name' => 'Panjabi; Punjabi'
        ]);
        DB::table('languages')->insert([
            'name' => 'Pali'
        ]);
        DB::table('languages')->insert([
            'name' => 'Polish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Pushto; Pashto'
        ]);
        DB::table('languages')->insert([
            'name' => 'Portuguese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Quechua'
        ]);
        DB::table('languages')->insert([
            'name' => 'Romansh'
        ]);
        DB::table('languages')->insert([
            'name' => 'Rundi'
        ]);
        DB::table('languages')->insert([
            'name' => 'Romanian; Moldavian; Moldovan'
        ]);
        DB::table('languages')->insert([
            'name' => 'Russian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Kinyarwanda'
        ]);
        DB::table('languages')->insert([
            'name' => 'Sanskrit'
        ]);
        DB::table('languages')->insert([
            'name' => 'Sardinian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Sindhi'
        ]);
        DB::table('languages')->insert([
            'name' => 'Northern Sami'
        ]);
        DB::table('languages')->insert([
            'name' => 'Sango'
        ]);
        DB::table('languages')->insert([
            'name' => 'Sinhala; Sinhalese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Slovak'
        ]);
        DB::table('languages')->insert([
            'name' => 'Slovenian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Samoan'
        ]);
        DB::table('languages')->insert([
            'name' => 'Shona'
        ]);
        DB::table('languages')->insert([
            'name' => 'Somali'
        ]);
        DB::table('languages')->insert([
            'name' => 'Albanian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Serbian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Swati'
        ]);
        DB::table('languages')->insert([
            'name' => 'Sotho, Southern'
        ]);
        DB::table('languages')->insert([
            'name' => 'Sundanese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Swedish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Swahili'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tamil'
        ]);
        DB::table('languages')->insert([
            'name' => 'Telugu'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tajik'
        ]);
        DB::table('languages')->insert([
            'name' => 'Thai'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tigrinya'
        ]);
        DB::table('languages')->insert([
            'name' => 'Turkmen'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tagalog'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tswana'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tonga (Tonga Islands)'
        ]);
        DB::table('languages')->insert([
            'name' => 'Turkish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tsonga'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tatar'
        ]);
        DB::table('languages')->insert([
            'name' => 'Twi'
        ]);
        DB::table('languages')->insert([
            'name' => 'Tahitian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Uighur; Uyghur'
        ]);
        DB::table('languages')->insert([
            'name' => 'Ukrainian'
        ]);
        DB::table('languages')->insert([
            'name' => 'Urdu'
        ]);
        DB::table('languages')->insert([
            'name' => 'Uzbek'
        ]);
        DB::table('languages')->insert([
            'name' => 'Venda'
        ]);
        DB::table('languages')->insert([
            'name' => 'Vietnamese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Volapük'
        ]);
        DB::table('languages')->insert([
            'name' => 'Walloon'
        ]);
        DB::table('languages')->insert([
            'name' => 'Wolof'
        ]);
        DB::table('languages')->insert([
            'name' => 'Xhosa'
        ]);
        DB::table('languages')->insert([
            'name' => 'Yiddish'
        ]);
        DB::table('languages')->insert([
            'name' => 'Yoruba'
        ]);
        DB::table('languages')->insert([
            'name' => 'Zhuang; Chuang'
        ]);
        DB::table('languages')->insert([
            'name' => 'Chinese'
        ]);
        DB::table('languages')->insert([
            'name' => 'Zulu'
        ]);
    }
}
