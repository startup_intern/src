<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'System Developer',
        ]);
        DB::table('categories')->insert([
            'name' => 'Web Designer',
        ]);
        DB::table('categories')->insert([
            'name' => 'Web Marketer',
        ]);
        DB::table('categories')->insert([
            'name' => 'Translator',
        ]);
    }
}
