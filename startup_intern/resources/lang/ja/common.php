<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'header' => [
        'home_button' => 'ホーム',
        'login_button' => 'ログイン',
    ],
    'footer' => [
        'home_button' => 'ホーム',
        'login_button' => 'ログイン',
    ],


];
