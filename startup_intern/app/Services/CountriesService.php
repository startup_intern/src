<?php
/**
 * Created by PhpStorm.
 * User: masahirohanawa
 * Date: 7/27/16
 * Time: 5:23 PM
 */

namespace app\Services;
use App\Models\Countries;
use App\Repositories\CountriesRepository;


class CountriesService
{

    public function __construct(CountriesRepository $countriesRepository)
    {
        $this->countriesRepository = $countriesRepository;
    }

    public function getCountryList($user = null) {
        $this->countriesRepository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $countries = $this->countriesRepository->all();
        return $countries;
    }

}