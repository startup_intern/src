<?php
/**
 * Created by PhpStorm.
 * User: masahirohanawa
 * Date: 7/27/16
 * Time: 5:23 PM
 */

namespace app\Services;
use App\Models\Roles;
use App\Repositories\RolesRepository;
use App\Http\Requests\RolesUpdateRequest;


class RolesService
{

    public function __construct(RolesRepository $rolesRepository)
    {
        $this->rolesRepository = $rolesRepository;
    }

    public function getRoleList($user = null)
    {
        $this->rolesRepository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $roles = $this->rolesRepository->all();
        return $roles;
    }
    
    public function createRole($request)
    {
        return $this->rolesRepository->create($request);
    }


}