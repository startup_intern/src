<?php
/**
 * Created by PhpStorm.
 * User: masahirohanawa
 * Date: 7/27/16
 * Time: 5:23 PM
 */

namespace app\Services;
use App\Models\Languages;


class LangApiService
{

    public function __construct(Languages $languages)
    {
        $this->languages = $languages;
    }

    public function getLangData($locale, $path) {
        return \Lang::get($path, array(), $locale);
    }

}