<?php
/**
 * Created by PhpStorm.
 * User: masahirohanawa
 * Date: 7/27/16
 * Time: 5:23 PM
 */

namespace app\Services;
use App\Http\Requests\Request;
use App\Repositories\UsersRepository;
use App\Repositories\StudentProfilesRepository;
use App\Repositories\StartupProfilesRepository;
use App\Repositories\CompanyProfilesRepository;
use App\Repositories\UserLanguagesRepository;
use App\Repositories\UserRolesRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;


class UsersService
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    public static $STUDENT = 1;
    public static $STARTUP = 2;
    public static $COMPANY = 3;
    public static $TEMP_REGISTER = 1;
    public static $REGISTER = 2;

    public function __construct(
        UsersRepository $usersRepository,
        StudentProfilesRepository $studentProfilesRepository,
        StartupProfilesRepository $startupProfilesRepository,
        CompanyProfilesRepository $companyProfilesRepository,
        UserLanguagesRepository $userLanguagesRepository,
        UserRolesRepository $userRolesRepository
    )
    {
        $this->usersRepository = $usersRepository;
        $this->studentProfilesRepository = $studentProfilesRepository;
        $this->startupProfilesRepository = $startupProfilesRepository;
        $this->companyProfilesRepository = $companyProfilesRepository;
        $this->userLanguagesRepository = $userLanguagesRepository;
        $this->userRolesRepository = $userRolesRepository;
    }


    public function findUser($request)
    {
        try {
            $token = explode(' ', $request->header('authorization'));
            $user = JWTAuth::toUser($token[1]);
            $new_token  = JWTAuth::refresh($token[1]);

            if ( $user->auth_id == static::$STUDENT )
            {
                $profile = $this->studentProfilesRepository->findWhere([
                    'user_id' => $user->id
                ])->first();
            }
            else if ( $user->auth_id == static::$STARTUP )
            {
                $profile = $this->startupProfilesRepository->findWhere([
                    'user_id' => $user->id
                ])->first();
            }
            else if ( $user->auth_id == static::$COMPANY )
            {
                $profile = $this->companyProfilesRepository->findWhere([
                    'user_id' => $user->id
                ])->first();
            }

            $userData = [
                'user' => $user->toArray(),
                'profile' => $profile->toArray(),
                'token' => $new_token
            ];
        } catch (UserServiceException $e) {
            $userData = [
                'error_no' => 500,
                'msg' => 'Could not find user'
            ];
        }

        return $userData;
    }

    public function createUser($request)
    {

        $user = DB::transaction(function ($request) use ($request)
        {
            try {
                $userParam = [
                    'email' => $request->email,
                    'password' => $request->password,
                    'auth_id' => $request->authId,
                    'status' => static::$TEMP_REGISTER,
                    'facebook_id' => !empty($request->facebookId) ? $request->facebookId : '',
                    'facebook_token' => !empty($request->facebookToken) ? $request->facebookToken : '',
                    'user_token' => $this->get_csrf_token()
                ];
                $user = $this->usersRepository->create($userParam);

                if (!empty($user))
                {
                    if (!empty($request->language))
                    {
                        foreach($request->language as $language)
                        {
                            $userLanguage = [
                                'user_id' => $user->id,
                                'language_id' => $language['id']
                            ];
                            $this->userLanguagesRepository->create($userLanguage);
                        }
                    }

                    if (!empty($request->role))
                    {
                        foreach($request->role as $role) {
                            $userRole = [
                                'user_id' => $user->id,
                                'role_id' => $role['id']
                            ];
                            $this->userRolesRepository->create($userRole);
                        }
                    }

                    if (!empty($request->photo))
                    {
                        $pathData = explode('/', $request->photo);
                        $currentDomain = $pathData[0] . '//' . $pathData[2];
                        $realPath =  base_path() . '/public/' . $pathData[3] . '/' . $pathData[4] . '/' . $pathData[5];
                        if ($currentDomain == $request->root()) {
                            if (File::makeDirectory(base_path() . '/public/img/profile/' . $user->id, 0777, true))
                            {
                                if (File::move(
                                    $realPath,
                                    base_path() . '/public/img/profile/' . $user->id . '/' . $pathData[5]
                                ))
                                {
                                    $photo = $request->root() . '/img/profile/' . $user->id . '/' . $pathData[5];
                                    File::deleteDirectory($realPath);
                                }
                            }
                        } else {
                            $photo = $request->photo;
                        }
                    }

                    if ($user->auth_id == static::$STUDENT)
                    {
                        $studentProfile = [
                            'user_id' => $user->id,
                            'first_name' => !empty($request->firstName) ? $request->firstName : '',
                            'last_name' => !empty($request->lastName) ? $request->lastName : '',
                            'category_id' => !empty($request->categoryId) ? $request->categoryId : '',
                            'country_id' => !empty($request->countryId) ? $request->countryId : '',
                            'university_name' => !empty($request->universityName) ? $request->universityName : '',
                            'university_address' => !empty($request->universityAddress) ? $request->universityAddress : '',
                            'photo' => !empty($photo) ? $photo : '',
                            'resume' => !empty($request->resume) ? $request->resume : '',
                            'video' => !empty($request->video) ? $request->video : '',
                            'facebook' => !empty($request->facebook) ? $request->facebook : '',
                            'linkedin' => !empty($request->linkedin) ? $request->linkedin : '',
                            'github' => !empty($request->github) ? $request->github : '',
                            'website' => !empty($request->website) ? $request->website : '',
                            'skype_id' => !empty($request->skypeId) ? $request->skypeId : '',
                        ];
                        $profile = $this->studentProfilesRepository->create($studentProfile);
                    }
                    else if ($user->auth_id == static::$STARTUP)
                    {
                        $profile = $this->studentProfilesRepository->create($request);
                    }
                    else if ($user->auth_id == static::$COMPANY)
                    {
                        $profile = $this->studentProfilesRepository->create($request);
                    }

                    DB::commit();
                    
                    $userData = [
                        'user' => $user->toArray(),
                        'profile' => $profile->toArray()
                    ];

                    return $userData;

                }
            } catch (UserServiceException $e) {
                $userData = [
                    'error_no' => 500,
                    'msg' => 'Could not create user'
                ];
            }
        });

        if (empty($user['error_no'])) {
            Mail::send('emails.register', ['user' => $user], function ($m) use ($user) {
                $m->from('masahirohanawa0405@gmail.com', 'startup intern');

                $m->to($user['user']['email'])->subject('Important: verify your email');
            });
        }

        return $user;
    }

  /*
  * update user
  * @param $user User
  * @param $facebookUser string
  * @param $request UserRequest
  * @return User
  */
    public function updateUser($user, $facebookToken, $request)
    {

        $user = DB::transaction(function ($user, $request, $facebookToken) use ($user, $request, $facebookToken)
        {
            try {
                $userParam = [
                    'email' => !empty($request->email) ? $request->email : $user->email,
                    'password' => !empty($request->password) ? $request->password : null,
                    'facebook_id' => !empty($request->facebookId) ? $request->facebookId : '',
                    'facebook_token' => !empty($facebookToken) ? $facebookToken : $user->facebook_id
                ];
                $user = $this->usersRepository->update($userParam, $user->id);

                if (!empty($user) && !empty($request))
                {
                    // delete old user languages
                    $this->userLanguagesRepository->deleteWhere([
                        'user_id' => $user->id
                    ]);

                    // create new user languages
                    foreach($request->language as $language)
                    {
                        $userLanguage = [
                            'user_id' => $user->id,
                            'language_id' => $language['id']
                        ];
                        $this->userLanguagesRepository->create($userLanguage);
                    }

                    // delete old user role
                    $this->userRolesRepository->deleteWhere([
                        'user_id' => $user->id
                    ]);

                    // create new user role
                    foreach($request->role as $role) {
                        $userRole = [
                            'user_id' => $user->id,
                            'role_id' => $role['id']
                        ];
                        $this->userRolesRepository->create($userRole);
                    }

                    if (!empty($request->photo))
                    {
                        $pathData = explode('/', $request->photo);
                        $currentDomain = $pathData[0] . '//' . $pathData[2];
                        $realPath =  base_path() . '/public/' . $pathData[3] . '/' . $pathData[4] . '/' . $pathData[5];
                        if ($currentDomain == $request->root()) {
                            if (File::makeDirectory(base_path() . '/public/img/profile/' . $user->id, 0777, true))
                            {
                                if (File::move(
                                    $realPath,
                                    base_path() . '/public/img/profile/' . $user->id . '/' . $pathData[5]
                                ))
                                {
                                    $photo = $request->root() . '/img/profile/' . $user->id . '/' . $pathData[5];
                                    File::deleteDirectory($realPath);
                                }
                            }
                        } else {
                            $photo = $request->photo;
                        }
                    }

                    if ($user->auth_id == static::$STUDENT)
                    {

                        $studentProfileId = $this->studentProfilesRepository->findWhere([
                            'user_id' => $user->id
                        ])->first('id');

                        $studentProfile = [
                            'first_name' => !empty($request->firstName) ? $request->firstName : '',
                            'last_name' => !empty($request->lastName) ? $request->lastName : '',
                            'category_id' => !empty($request->categoryId) ? $request->categoryId : '',
                            'country_id' => !empty($request->countryId) ? $request->countryId : '',
                            'university_name' => !empty($request->universityName) ? $request->universityName : '',
                            'university_address' => !empty($request->universityAddress) ? $request->universityAddress : '',
                            'photo' => !empty($photo) ? $photo : '',
                            'resume' => !empty($request->resume) ? $request->resume : '',
                            'video' => !empty($request->video) ? $request->video : '',
                            'facebook' => !empty($request->facebook) ? $request->facebook : '',
                            'linkedin' => !empty($request->linkedin) ? $request->linkedin : '',
                            'github' => !empty($request->github) ? $request->github : '',
                            'website' => !empty($request->website) ? $request->website : '',
                            'skype_id' => !empty($request->skypeId) ? $request->skypeId : '',
                        ];
                        $profile = $this->studentProfilesRepository->update(
                            $studentProfile,
                            $studentProfileId
                        );
                    }
                    else if ($user->auth_id == static::$STARTUP)
                    {
                        $profile = $this->studentProfilesRepository->create($request);
                    }
                    else if ($user->auth_id == static::$COMPANY)
                    {
                        $profile = $this->studentProfilesRepository->create($request);
                    }

                }

                DB::commit();

                $userData = [
                    'user' => $user->toArray(),
                    'profile' => !empty($profile) ? $profile->toArray() : null
                ];

                return $userData;
            } catch (UserServiceException $e) {
                $userData = [
                    'error_no' => 500,
                    'msg' => 'Could not create user'
                ];
            }
        });

        return $user;
    }

    /*
     * update user
     * @param $request
     */
    function updateStatusUser($user_token)
    {
        $user = DB::transaction(function ($user_token) use ($user_token)
        {
            try {

                $user = $this->usersRepository->findWhere(
                    [
                        'user_token' => $user_token,
                        'status' => static::$TEMP_REGISTER
                    ]
                )->first();

                if ($user->id)
                {
                    $userParam = [
                        'status' => static::$REGISTER
                    ];
                    $user = $this->usersRepository->update($userParam, $user->id);

                    DB::commit();
                }

                return $user;

            } catch (UserServiceException $e) {
                DB::rollBack();
                $user = [
                    'error_no' => 500,
                    'msg' => 'Could not update user'
                ];
            }
        });

        if (! $token = JWTAuth::fromUser($user))
        {
            $userData = [
                'error_no' => 401,
                'msg' => 'Invalid credentials'
            ];
        }
        else
        {

            if ($user->auth_id == static::$STUDENT) {
                $profile = $this->studentProfilesRepository->findWhere([
                    'user_id' => $user->id
                ])->first();
            } else if ($user->auth_id == static::$STARTUP) {
                $profile = $this->startupProfilesRepository->findWhere([
                    'user_id' => $user->id
                ])->first();
            } else if ($user->auth_id == static::$COMPANY) {
                $profile = $this->companyProfilesRepository->findWhere([
                    'user_id' => $user->id
                ])->first();
            }

            $userData = [
                'user' => $user->toArray(),
                'profile' => $profile->toArray(),
                'token' => $token
            ];
        }

        return $userData;
    }

    /*
     * make CSRF token
     */
    function get_csrf_token()
    {
        $TOKEN_LENGTH = 16;
        $bytes = openssl_random_pseudo_bytes($TOKEN_LENGTH);
        return bin2hex($bytes);
    }

    /*
     * auth user
     */
    public function authUser($user, $request = null)
    {
        try {
            if (!empty($user))
            {
                $token = JWTAuth::fromUser($user);
            }
            else
            {
                $credentials = $request->only(['email', 'password']);
                $token = JWTAuth::attempt($credentials);
                if ($token)
                {
                    $user = $this->usersRepository->findWhere([
                        'email' => $request->email,
                        'status' => static::$REGISTER
                    ])->first();
                }
            }
            // attempt to verify the credentials and create a token for the user
            if (!$token) {
                return [
                    'msg' => 'invalid_credentials',
                    'error_no' => 401
                ];
            } else {
                if ( $user->auth_id == static::$STUDENT )
                {
                    $profile = $this->studentProfilesRepository->findWhere([
                        'user_id' => $user->id
                    ])->first();
                }
                else if ( $user->auth_id == static::$STARTUP )
                {
                    $profile = $this->startupProfilesRepository->findWhere([
                        'user_id' => $user->id
                    ])->first();
                }
                else if ( $user->auth_id == static::$COMPANY )
                {
                    $profile = $this->companyProfilesRepository->findWhere([
                        'user_id' => $user->id
                    ])->first();
                }
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return [
                'msg' => 'could_not_create_token',
                'error_no' => 500
            ];
        }

        return [
            'token' => $token,
            'user' => $user->toArray(),
            'profile' => !empty($profile) ? $profile->toArray() : null
        ];
    }

    /**
     * get user information by Facebook id
     * @param string $facebookId
     * @return User $user
     */
    public function getUserByFacebookId($facebookId)
    {

        $user = $this->usersRepository->findWhere(
            [
                'facebook_id' => $facebookId,
                'status' => static::$REGISTER
            ]
        )->first();

        return $user;
    }

    /**
     * check facebook user
     * @param Request $request
     * @return User $user
     */
    public function checkFacebookUser($request)
    {

        $result = Curl::to('https://graph.facebook.com/me?access_token=' . $request->facebookToken)->get();
        $facebookUserData = json_decode($result);

        if ( $facebookUserData->id == $request->facebookId )
        {
            $result = true;
        }
        else
        {
            $result = false;
        }

        return $result;
    }

    /**
     * get user information by Email
     * @param string $email
     * @return User $user
     */
    public function getUserByEmail($email)
    {

        $user = $this->usersRepository->findWhere(
            [
                'email' => $email,
                'status' => static::$REGISTER
            ]
        )->first();

        return $user;
    }

    /**
     * file upload
     * @param UserRequest $request
     * @return User $user
     */
    public function fileUpload($request)
    {

        $fileName = $request->file('photo')->getClientOriginalName();

        $TOKEN_LENGTH = 16;
        $randomDirectory = str_random($TOKEN_LENGTH);

        $result = File::makeDirectory(base_path() . '/public/temp/' . $randomDirectory, 0777, true);

        if ($result)
        {
            $request->file('photo')->move(
                base_path() . '/public/temp/' . $randomDirectory, $fileName
            );
            $result = [
                'path' => $request->root() . '/temp/' . $randomDirectory,
                'fileName' => $fileName
            ];
        }
        else
        {
            $result = [
                'path' => './temp/' . $randomDirectory,
                'fileName' => $fileName
            ];
        }

        return $result;

    }


}