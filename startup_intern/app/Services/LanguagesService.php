<?php
/**
 * Created by PhpStorm.
 * User: masahirohanawa
 * Date: 7/27/16
 * Time: 5:23 PM
 */

namespace app\Services;
use App\Models\Languages;
use App\Repositories\LanguagesRepository;


class LanguagesService
{

    public function __construct(LanguagesRepository $languagesRepository)
    {
        $this->languagesRepository = $languagesRepository;
    }

    public function getLanguageList($user = null) {
        $this->languagesRepository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $languages = $this->languagesRepository->all();
        return $languages;
    }

}