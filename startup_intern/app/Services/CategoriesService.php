<?php
/**
 * Created by PhpStorm.
 * User: masahirohanawa
 * Date: 7/27/16
 * Time: 5:23 PM
 */

namespace app\Services;
use App\Models\Categories;
use App\Repositories\CategoriesRepository;


class CategoriesService
{

    public function __construct(CategoriesRepository $categoriesRepository)
    {
        $this->categoriesRepository = $categoriesRepository;
    }

    public function getCategoryList($user = null) {
        $this->categoriesRepository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $categories = $this->categoriesRepository->all();
        return $categories;
    }

}