<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentProfiles extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'student_profiles';

    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'category_id',
        'country_id',
        'university_name',
        'university_address',
        'photo',
        'resume',
        'video',
        'facebook',
        'linkedin',
        'github',
        'website',
        'skype_id',
        'status'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
