<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\LanguagesRepository::class, \App\Repositories\LanguagesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RolesRepository::class, \App\Repositories\RolesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UsersRepository::class, \App\Repositories\UsersRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StudentProfilesRepository::class, \App\Repositories\StudentProfilesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StartupProfilesRepository::class, \App\Repositories\StartupProfilesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CompanyProfilesRepository::class, \App\Repositories\CompanyProfilesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserLanguagesRepository::class, \App\Repositories\UserLanguagesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRolesRepository::class, \App\Repositories\UserRolesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CountriesRepository::class, \App\Repositories\CountriesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CategoriesRepository::class, \App\Repositories\CategoriesRepositoryEloquent::class);
        //:end-bindings:
    }
}
