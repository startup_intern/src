<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\UserLanguagesCriteria;

/**
 * Interface UserLanguagesRepository
 * @package namespace App\Repositories;
 */
class UserLanguagesRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
        $this->pushCriteria(new UserLanguagesCriteria());
    }

    function model(){
        return "App\\Models\\UserLanguages";
    }
}
