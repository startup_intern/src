<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\CompanyProfilesCriteria;

/**
 * Interface CompanyProfilesRepository
 * @package namespace App\Repositories;
 */
class CompanyProfilesRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
        $this->pushCriteria(new CompanyProfilesCriteria());
    }

    function model(){
        return "App\\Models\\CompanyProfiles";
    }
}
