<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LanguagesRepository;
use App\Models\Languages;
use App\Validators\LanguagesValidator;

/**
 * Class LanguagesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LanguagesRepositoryEloquent extends LanguagesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Languages::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
