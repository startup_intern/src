<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserLanguagesRepository;
use App\Models\UserLanguages;
use App\Validators\UserLanguagesValidator;

/**
 * Class UserLanguagesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserLanguagesRepositoryEloquent extends UserLanguagesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserLanguages::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
