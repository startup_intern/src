<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\StudentProfilesCriteria;

/**
 * Interface StudentProfilesRepository
 * @package namespace App\Repositories;
 */
class StudentProfilesRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
        $this->pushCriteria(new StudentProfilesCriteria());
    }

    function model(){
        return "App\\Models\\StudentProfiles";
    }
}
