<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\LanguagesCriteria;

/**
 * Interface LanguagesRepository
 * @package namespace App\Repositories;
 */
class LanguagesRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
        $this->pushCriteria(new LanguagesCriteria());
    }

    function model(){
        return "App\\Models\\Languages";
    }
}
