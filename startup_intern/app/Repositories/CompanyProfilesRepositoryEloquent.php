<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CompanyProfilesRepository;
use App\Models\CompanyProfiles;
use App\Validators\CompanyProfilesValidator;

/**
 * Class CompanyProfilesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CompanyProfilesRepositoryEloquent extends CompanyProfilesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompanyProfiles::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CompanyProfilesValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
