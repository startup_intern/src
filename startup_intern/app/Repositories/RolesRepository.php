<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\RolesCriteria;

/**
 * Interface RolesRepository
 * @package namespace App\Repositories;
 */
class RolesRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
        $this->pushCriteria(new RolesCriteria());
    }

    function model(){
        return "App\\Models\\Roles";
    }
}
