<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\StartupProfilesCriteria;

/**
 * Interface StartupProfilesRepository
 * @package namespace App\Repositories;
 */
class StartupProfilesRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
        $this->pushCriteria(new StartupProfilesCriteria());
    }

    function model(){
        return "App\\Models\\StartupProfiles";
    }
}
