<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\UserRolesCriteria;
/**
 * Interface UserRolesRepository
 * @package namespace App\Repositories;
 */
class UserRolesRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
        $this->pushCriteria(new UserRolesCriteria());
    }

    function model(){
        return "App\\Models\\UserRoles";
    }
}
