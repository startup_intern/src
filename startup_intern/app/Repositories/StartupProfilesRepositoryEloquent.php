<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\StartupProfilesRepository;
use App\Models\StartupProfiles;
use App\Validators\StartupProfilesValidator;

/**
 * Class StartupProfilesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class StartupProfilesRepositoryEloquent extends BaseRepository implements StartupProfilesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return StartupProfiles::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return StartupProfilesValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
