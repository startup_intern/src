<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\UsersCriteria;

/**
 * Interface UsersRepository
 * @package namespace App\Repositories;
 */
class UsersRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
        $this->pushCriteria(new UsersCriteria());
    }

    function model(){
        return "App\\Models\\Users";
    }
}
