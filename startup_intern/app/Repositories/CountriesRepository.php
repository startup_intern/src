<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\CountriesCriteria;
/**
 * Interface CountriesRepository
 * @package namespace App\Repositories;
 */
class CountriesRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
    $this->pushCriteria(new CountriesCriteria());
}

    function model(){
        return "App\\Models\\Countries";
    }
}
