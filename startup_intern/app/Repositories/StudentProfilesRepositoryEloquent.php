<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\StudentProfilesRepository;
use App\Models\StudentProfiles;
use App\Validators\StudentProfilesValidator;

/**
 * Class StudentProfilesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class StudentProfilesRepositoryEloquent extends StudentProfilesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return StudentProfiles::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return StudentProfilesValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
