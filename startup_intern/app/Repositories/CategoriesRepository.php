<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Criteria\CategoriesCriteria;

/**
 * Interface CategoriesRepository
 * @package namespace App\Repositories;
 */
class CategoriesRepository extends BaseRepository implements RepositoryInterface
{
    public function boot(){
        $this->pushCriteria(new CategoriesCriteria());
    }

    function model(){
        return "App\\Models\\Categories";
    }
}

