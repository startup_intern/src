<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserRolesRepository;
use App\Models\UserRoles;
use App\Validators\UserRolesValidator;

/**
 * Class UserRolesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRolesRepositoryEloquent extends UserRolesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserRoles::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
