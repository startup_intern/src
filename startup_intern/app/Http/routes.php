<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/logout', function () {
    return view('index');
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    /* API */
    $api->group(['prefix' => 'v1',
        'before' => 'v1',
        'middleware' => 'cors',
        'namespace' => 'App\\Http\\Controllers',
        'domain' => '192.168.33.10'
    ],function ($api) {

        // get language list
        $api->get('/language/', "LanguagesController@index");

        // get role list
        $api->get('/role/', "RolesController@index");

        // get category list
        $api->get('/category/', "CategoriesController@index");
        
        // get country list
        $api->get('/country/', "CountriesController@index");

        // add language list
        $api->post('/role/', "RolesController@store");

        // add user
        $api->post('/user/', "UsersController@store");

        // user register check
        $api->get('/user/register/check', "UsersController@checkRegisterUser");

        // user login
        $api->post('/user/login', "Auth\UsersLoginController@store");

        // facebook login
        $api->post('/user/facebook/login','Auth\UsersFacebookController@facebookLogin');

        // upload image
        $api->post('/upload/image','UsersController@uploadImage');

        // upload document
        $api->post('/upload/document','UsersController@uploadDocument');

        // update user status
        $api->put('/user/accept/{userToken}', "UsersController@statusUpdate");
    });

    //protected API routes with JWT (must be logged in)
    $api->group([
        'prefix' => 'v1',
        'before' => 'v1',
        'middleware' => ['jwt.auth', 'cors'],
        'namespace' => 'App\\Http\\Controllers',
        'domain' => '192.168.33.10'
    ],function ($api) {
        $api->get('/user/check', "UsersController@showUser");
    });
});

