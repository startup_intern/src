<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\LanguagesCreateRequest;
use App\Http\Requests\LanguagesUpdateRequest;
use App\Validators\LanguagesValidator;
use App\Services\LanguagesService;


class LanguagesController extends Controller
{

    /**
     * @var LanguagesRepository
     */
    protected $repository;

    /**
     * @var LanguagesValidator
     */
    protected $validator;

    public function __construct(LanguagesService $languagesService, LanguagesValidator $validator)
    {
        $this->languagesService = $languagesService;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $languages = $this->languagesService->getLanguageList();

        return response()->json([
            'data' => $languages,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LanguagesCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(LanguagesCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            
            $language = $this->repository->create($request->all());

            $response = [
                'message' => 'Languages created.',
                'data'    => $language->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $language = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $language,
            ]);
        }

        return view('languages.show', compact('language'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $language = $this->repository->find($id);

        return view('languages.edit', compact('language'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  LanguagesUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(LanguagesUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $language = $this->repository->update($id, $request->all());

            $response = [
                'message' => 'Languages updated.',
                'data'    => $language->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Languages deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Languages deleted.');
    }
}
