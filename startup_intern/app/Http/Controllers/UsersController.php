<?php

namespace App\Http\Controllers;

use App\Validators\UploadImageValidator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UsersCreateRequest;
use App\Http\Requests\UsersUpdateRequest;
use App\Repositories\UsersRepository;
use App\Validators\UsersValidator;
use App\Validators\UsersLoginValidator;
use App\Services\UsersService;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Log;


class UsersController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * @var UsersRepository
     */
    protected $repository;

    /**
     * @var UsersValidator
     */
    protected $validator;

    public function __construct(
        UsersService $usersService,
        UsersValidator $validator,
        UsersLoginValidator $usersLoginValidator,
        UploadImageValidator $uploadImageValidator
    )
    {
        $this->usersService = $usersService;
        $this->validator  = $validator;
        $this->usersLoginValidator  = $usersLoginValidator;
        $this->uploadImageValidator  = $uploadImageValidator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->usersService->getUser();
        $users = $this->repository->all();

        return response()->json([
            'data' => $users,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UsersCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UsersCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $user = $this->usersService->createUser($request);

            $response = [
                'message' => 'Users created.',
                'data'    => $user,
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {

            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }

    /**
     * check active user by email.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkRegisterUser(UsersCreateRequest $request)
    {
        try {

            $user = $this->usersService->getUserByEmail($request->email);

            return response()->json([
                'data' => $user,
            ]);

        } catch (ValidatorException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showUser(UsersCreateRequest $request)
    {
        try {
            
            $user = $this->usersService->findUser($request);

            return response()->json([
                'data' => $user,
            ]);

        } catch (ValidatorException $e) {
            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = $this->repository->find($id);

        return view('users.edit', compact('user'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  UsersUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function statusUpdate($user_token)
    {

        try {

            $user = $this->usersService->updateStatusUser($user_token);

            $response = [
                'message' => 'Users updated.',
                'data'    => $user,
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {

            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UsersUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(UsersUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $user = $this->repository->update($id, $request->all());

            $response = [
                'message' => 'Users updated.',
                'data'    => $user->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Users deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Users deleted.');
    }


    /**
     * upload Image
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(UsersCreateRequest $request)
    {
        try {

            Log::Info($request->photo);
            $this->uploadImageValidator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $file = $this->usersService->fileUpload($request);

            $response = [
                'message' => 'Image updated.',
                'data'    => $file,
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {

            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }


}
