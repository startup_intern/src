<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UsersCreateRequest;
use App\Http\Requests\UsersUpdateRequest;
use App\Http\Controllers\Controller;
use App\Repositories\UsersRepository;
use App\Validators\UsersValidator;
use App\Validators\UsersLoginValidator;
use App\Services\UsersService;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Laravel\Socialite\Contracts\Factory as Socialite;


class UsersFacebookController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * @var UsersRepository
     */
    protected $repository;

    /**
     * @var UsersValidator
     */
    protected $validator;

    public function __construct(UsersService $usersService, UsersValidator $validator, Socialite $socialite)
    {
        $this->usersService = $usersService;
        $this->validator  = $validator;
        $this->socialite = $socialite;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UsersCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UsersCreateRequest $request)
    {

        try {

            $token = $this->usersService->authUser($request);

            $response = [
                'data'    => $token,
            ];

            return response()->json($response);

        } catch (ValidatorException $e) {

            return response()->json([
                'error'   => true,
                'message' => $e->getMessageBag()
            ]);
        }
    }

    /**
     * redirect facebook page and get these information
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function facebookLogin(UsersCreateRequest $request)
    {

        $user = $this->usersService->getUserByFacebookId($request->facebookId);

        $result = $this->usersService->checkFacebookUser($request);

        if (!$result) {
            $response = [
                'error_no' => 999,
                'msg' => 'Can\'t match facebook user'
            ];

            return response()->json($response);
        }

        if (!empty($user) && $result)
        {
            $token = $this->usersService->authUser($user);

            $response = [
                'data'    => $token,
            ];

            return response()->json($response);
        }
        else
        {
            $user = $this->usersService->getUserByEmail($request->email);
            if (!empty($user) && $result)
            {
                $token = $this->usersService->updateUser($user, $request->facebookToken, null);
                $response = [
                    'data'    => $token,
                ];

                return response()->json($response);
            }
            else
            {
                $response = [
                    'error_no' => 500,
                    'msg' => 'Could not create user'
                ];

                return response()->json($response);
            }
        }
    }

}
