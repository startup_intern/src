<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UploadImageValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'photo' => 'required|image|max:2097152'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'userToken' => 'required|exists:users,user_token,status,1|unique:users'
        ],
   ];
}
