<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UploadFileValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'resume' => 'required|size:2097152,'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'userToken' => 'required|exists:users,user_token,status,1|unique:users'
        ],
   ];
}
