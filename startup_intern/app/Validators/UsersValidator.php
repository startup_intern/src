<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UsersValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email' => 'required|email|max:255|unique:users,email,NULL,id,status,2,deleted_at,NULL',
            'password' => 'required|min:6|confirmed',
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'countryId' => 'required|max:255',
            'resume' => 'required',
            'video' => 'required|max:255',
            'skypeId' => 'required|max:255'
        ],
        ValidatorInterface::RULE_UPDATE => [
            
        ],
   ];
}
