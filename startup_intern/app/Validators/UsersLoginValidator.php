<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class UsersLoginValidator extends LaravelValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email' => 'required|exists:users,email,deleted_at,NULL|required_if:status,2',
            'password' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'userToken' => 'required|exists:users,user_token,status,1|unique:users'
        ],
   ];
}
